import React from "react";

class GameInfo extends React.Component {
  render() {
    return <h3>It is your turn <div style={{color: this.props.color}}>{this.props.currentPlayer}</div></h3>
  }
}

export default GameInfo;
