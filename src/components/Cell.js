import React from "react";

const cellStyle = {
  display: "block",
  backgroundColor: "white",
  width: "200px",
  height: "200px",
  border: "1px solid #333",
  outline: "none",
  textAlign: "center",
  lineHeight: "200px",
  cursor: "pointer"
};

class Cell extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: "",
      color: "white",
      currentPlayer: "",
    };
  }

  mouseOver(event) {
    if(this.state.color === "white") {
      this.setState({color: "grey"});
      event.target.style.backgroundColor = "grey";
    }
  }

  mouseOut(event) {
    if(this.state.color === "grey") {
      this.setState({color: "white"});
      event.target.style.backgroundColor = "white";
    }
  }

  mouseClick(event) {
    if(this.state.color === "grey" || this.state.color === "white") {
      if(this.props.color === "red") {
        this.setState({color: this.props.color, value:"O"});
        event.target.style.backgroundColor = this.props.color;
      }
      else {
        this.setState({color: this.props.color, value:"X"});
        event.target.style.backgroundColor = this.props.color;
      }
      this.props.onClick();
    }
  }

  render() {
    return (<div style={cellStyle}
      onClick={e => this.mouseClick(e)}
      onMouseOver={e => this.mouseOver(e)}
      onMouseOut={e => this.mouseOut(e)}>{this.state.value}</div>);
  }
}

export default Cell;
