import React from "react";
import Cell from "./Cell";

const boardStyle = {
  display: "grid",
  width: "600px",
  height: "calc(100%)",
  grid: "auto-flow dense / 1fr 1fr 1fr",
  gridAutoRows: "auto"
};

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cells: [0,1,2,3,4,5,6,7,8],
    }
    console.log(this.state.cells);
  }

  render() {
    return (
      <div style={boardStyle}>{this.state.cells.map(c =>
        <Cell key={c} player={this.props.currentPlayer} color={this.props.color} onClick={this.props.onClick}/>
      )}</div>
    )
  }
}

export default Board;
