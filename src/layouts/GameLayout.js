import React from "react";
import Board from "../components/Board";
import GameInfo from "../components/GameInfo";

const gameLayoutStyle = {
  width: 650,
  height: `calc(90%)`,
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  margin: "auto"
};

class GameLayout extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);

    this.state = {
      cells: Array(9).fill(null),
      currentPlayer: "playerOne",
      color: "blue",
    };
  }

  // getDerivedStateFromProps is called before every render,
  // use it to infer new state values from props or state changes.
  static getDerivedStateFromProps(props, state) {
    return state;
  }

  onClick() {
    if (this.state.currentPlayer === "playerOne") {
      this.setState({
        currentPlayer: "playerTwo",
        color: "red",
      });
    }
    else {
      this.setState({
        currentPlayer: "playerOne",
        color: "blue",
      });
    }
  }

  render() {
    return (
      <div
        style={gameLayoutStyle}
      >
        <GameInfo currentPlayer={this.state.currentPlayer} color={this.state.color}/>
        <Board currentPlayer={this.state.currentPlayer} color={this.state.color} onClick={this.onClick}/>
      </div>
    );
  }
}

export default GameLayout;
